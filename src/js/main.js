'use strict'

let count = 0;

let inputValues = {
    cocktailBarValue: '',
    firstNameValue: '',
    lastNameValue: '',
    emailValue: '',
    phoneNumberValue: ''
}

let errInputCocktail = document.querySelector('.errInputCocktail');
let errInputFirst = document.querySelector('.errInputFirst');
let errInputLast = document.querySelector('.errInputLast');
let errInputPhone = document.querySelector('.errInputPhone');
let errInputEmail = document.querySelector('.errInputEmail');
let btnNext = document.querySelector('#btnNext');
let cocktailBar = document.querySelector('#cocktail__bar');
let firstName = document.querySelector('#first__name');
let lastName = document.querySelector('#last__name');
let email = document.querySelector('#email');
let phoneNumber = document.querySelector('#mobile');
let firstDiv = document.querySelector('.first__div');
let progressBar = document.querySelector('.progress-bar');
let hiden1 = document.querySelector('.hiden1');
let hiden2 = document.querySelector('.hiden2');
let spiner = document.querySelector('.spiner');

let category = document.querySelector('#category');
let alcoholic = document.querySelector('#typeofdrink');
let glass = document.querySelector('#typeofglass');
let ingr = $("#ingredients");
let responseFinish;

/************************ FUNCTIONS ***********************/

$('#ingredients').select2({
    allowClear: true
});

const loadData = async (respName, url, selector, str) => {
    try {
        const respName = await axios(url);
        respName.data["drinks"].forEach(res => {
            let category = document.querySelector(selector);
            let option = document.createElement("option");
            option.text = res[str];
            option.value = res[str];
            category.appendChild(option);
        })
    } catch (err) {
        console.error(err.message)
    }
}

let init = () => {
    cocktailBar.focus();
    loadData('categoryResp', 'https://www.thecocktaildb.com/api/json/v1/1/list.php?c=list', '#category', 'strCategory');
    loadData('alcoholicResp', 'https://www.thecocktaildb.com/api/json/v1/1/list.php?a=list', '#typeofdrink', 'strAlcoholic');
    loadData('glassResp', 'https://www.thecocktaildb.com/api/json/v1/1/list.php?g=list', '#typeofglass', 'strGlass');
    loadData('ingredientsResp', 'https://www.thecocktaildb.com/api/json/v1/1/list.php?i=list', '#ingredients', 'strIngredient1')
}
init();

let sweetAlertFun = (input) => {
    Swal.fire({
        title: 'Empty field!',
        text: 'Plese enter ' + input,
        icon: 'error',
        confirmButtonText: 'OK',
        timer: 3000
    })
}

const emptyInput = (input, err) => {
    if (input.value === '') {
        err.textContent = 'empty input';
        err.style.fontSize = '1rem';
        err.style.color = 'red';
    } else {
        err.textContent = '';
    }
}

const cocktailValidate = (inputText) => {
    emptyInput(inputText, errInputCocktail);
}
const firstNameValidate = (inputText) => {
    emptyInput(inputText, errInputFirst);
}
const lastNameValidate = (inputText) => {
    emptyInput(inputText, errInputLast);
}
const emailValidate = (inputText) => {

    if (/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(inputText.value) === false) {
        errInputEmail.textContent = 'invalid email';
        errInputEmail.style.fontSize = '1rem';
        errInputEmail.style.color = 'red';
        if (inputText.value === '') {
            errInputEmail.textContent = 'empty input';
            errInputEmail.style.fontSize = '1rem';
            errInputEmail.style.color = 'red';
        }
    } else {
        errInputEmail.textContent = '';
    }
}
const phoneValidate = (inputText) => {
    emptyInput(inputText, errInputPhone);
}


function emptyField(input, input2, countNum) {
    if (input.value.length === 0) {
        count = countNum;
        sweetAlertFun(input2);
        e.preventDefault();
    }
}

/************************ EVENT LISTENERS ***********************/

btnNext.addEventListener('click',function (e) {
    count++;
    if (count === 1) {
        // store personal info
        inputValues.firstNameValue = firstName.value;
        inputValues.lastNameValue = lastName.value;
        inputValues.emailValue = email.value;
        inputValues.phoneNumberValue = phoneNumber.value;
        inputValues.cocktailBarValue = cocktailBar.value;

        // validation
        emptyField(cocktailBar, 'cocktail bar', 0);
        emptyField(firstName, 'first name', 0)
        emptyField(lastName, 'last name', 0)
        emptyField(phoneNumber, 'phone number', 0)

        if (/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email.value) === false || email.value.length === 0) {
            count = 0;
            Swal.fire({
                title: 'Empty field!',
                text: 'Email is empty or invalid',
                icon: 'error',
                confirmButtonText: 'OK',
                timer: 3000
            })
            return false;
        }

        // dom
        progressBar.style.width = '66.6666%';
        progressBar.textContent = '2 of 3';
        hiden1.style.visibility = 'hidden';
        spiner.style.display = 'block';

        setTimeout(function () {
            hiden1.style.display = 'none';
            spiner.style.display = 'none';
            hiden2.style.display = 'block';

        }, 400)
    } else if (count === 2) {
        count++;

        //validation
        emptyField(category, 'category', 1);
        emptyField(alcoholic, 'type', 1);
        emptyField(glass, 'glass type', 1);
        if (ingr.val().length === 0) {
            count = 1;
            sweetAlertFun('one or more ingredient')
            e.preventDefault();
            return false;
        }

        // style
        progressBar.style.width = '100%';
        progressBar.textContent = '3 of 3';

        // get the list of all cocktails and filter
        async function listAllCocktails(category, alcoholic, glassValue, ingredient) {
            try {
                let response = await fetch('https://www.thecocktaildb.com/api/json/v1/1/search.php?s');
                if (!response.ok) {
                    const message = `An error has occured: ${response.status}`;
                    throw new Error(message);
                }
                const list = (await response).json();

                list.then(list => {
                    responseFinish = list["drinks"].filter((res, i) => {
                        return res.strCategory === category && res.strAlcoholic === alcoholic && res.strGlass === glassValue && Object.values(res).includes(ingredient);
                    });
                });
            } catch (error) {
                console.error(error)
            }
        }

        listAllCocktails(category.value, alcoholic.value, glass.value, ...ingr.val());

        // style
        hiden2.style.display = 'none';
        spiner.style.display = 'block';

        setTimeout(function () {
            let html;

            firstDiv.style.height = '70vh';
            spiner.style.display = 'none';

            // show cocktails
            responseFinish.forEach(el => {
                html = `<div class="col-lg-3">
                            <img class="slika" data-id="${el.idDrink}" src="${el.strDrinkThumb}" style="cursor: pointer;">
                        </div>`;

                document.querySelector('.row1').insertAdjacentHTML('afterbegin', html);
            })
            document.querySelector('.row1').style.height = '35vh';
            btnNext.textContent = 'Search again!'
            // document.querySelector('.btnrow').style.display = 'none';
        }, 400)
    }
    if (count === 3) {
        setTimeout(function () {
            let html;
            let slikaAll = document.querySelectorAll('.slika');
            let modal = document.querySelector('.modal');

            // modal with cocktail info
            for (let i = 0; i < slikaAll.length; i++) {
                slikaAll[i].addEventListener('click', function () {
                    responseFinish.forEach(el => {
                        if (el.idDrink === this.dataset.id) {
                            modal.innerHTML = '';

                            html = `<div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title text-center" id="exampleModalCenterTitle">${el.strDrink.toUpperCase()}</h5>
                                                <button type="button" class="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                            <div class="bodyTxt">Category: ${el.strCategory}</div>
                                            <div class="bodyTxt">Type: ${el.strAlcoholic}</div>
                                            <div class="bodyTxt">Glass: ${el.strGlass}</div>
                                            Ingredients: ${el.strIngredient1 === null ? '' : el.strIngredient1}, ${el.strIngredient2 === null ? '' : el.strIngredient2}, ${el.strIngredient3 === null ? '' : el.strIngredient3}, ${el.strIngredient4 === null ? '' : el.strIngredient4}, ${el.strIngredient5 === null ? '' : el.strIngredient5}
                                            </div>
                                        </div>
                                    </div>`;

                            modal.insertAdjacentHTML('afterbegin', html)

                            $('.modal').modal('show');
                        }
                    })
                })
            }
            $('#btnNext').on('click', function () {
                location.reload();
            });

        }, 401)
    }
})
